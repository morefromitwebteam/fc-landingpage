'use strict';

(function ($) {
    var config = {
        newsroom: {
            api: 'https://newsroom.futurocoin.com/wp-json/wp/v2/posts',
            prepareData: function(response) {
                return response.map(function (item) {
                    return {
                        title: helpers.removeHtmlTags(item.title.rendered),
                        date: item.date,
                        link: item.link,
                        content: helpers.truncateText(item.excerpt.rendered, 150),
                        imageApi: item._links['wp:featuredmedia'][0].href
                    }
                });
            }
        },
        image: {
            prepareData: function (response) {
                return {
                    url: response.guid.rendered
                }
            }
        }
    },
    helpers = {
        removeHtmlTags: function (text) {
            return $('<div>').html(text).text();
        },

        truncateText: function (text, length) {
            return this.removeHtmlTags(text).substring(0, length).split(" ").slice(0, -1).join(" ") + "...";
        }
    },
    $el = {
        wrapper: $('.js-news'),
        container: $('.js-news-container'),
        newsTemplate: $('.js-news-container .js-news-template'),
        buttonMore: $('.js-news .js-news--button-more')
    },
    variables = {
        newsCurrentPage: 1,
        newsPerPage: 3
    },
    newsModule = (function () {
        var appendNews = function (data) {
            var $news;
            $news = data.map(function (item) {
                var $template = $el.newsTemplate.clone()
                    .removeClass('js-news-template')
                    .find('[data-news-title]').text(item.title).end()
                    .find('[data-news-url]').attr('href', item.link).end()
                    .find('[data-news-date]').text(moment(item.date).format('D MMM YYYY')).end()
                    .find('[data-news-content]').text(item.content).end();

                addImage($template.find('[data-news-image]'), item.imageApi);
                return $template;
            });
            $el.container.append($news);
        };

        var addImage = function ($image, imageApiUrl) {
            $.ajax({
                type: 'GET',
                url: imageApiUrl,

                success: function (response) {
                    var image = config.image.prepareData(response);
                    $image.attr('src', image.url);
                    $image.on('load', function () {
                        $image.addClass('news__image--loaded');
                    });
                }
            });
        };

        var preloadNews = function () {
            var queryParameters = {
                per_page: variables.newsPerPage,
                page: variables.newsCurrentPage
            };

            $.ajax({
                type: 'GET',
                url: config.newsroom.api + '?' + $.param(queryParameters),

                success: function (response) {
                    var newsData = config.newsroom.prepareData(response);
                    appendNews(newsData);
                    isMoreNewsAvailable();
                    variables.newsCurrentPage += 1;
                }
            });
        };

        var isMoreNewsAvailable = function () {
            var queryParameters = {
                per_page: variables.newsPerPage,
                page: variables.newsCurrentPage + 1
            };

            $.ajax({
                type: 'GET',
                url: config.newsroom.api + '?' + $.param(queryParameters),

                error: function (response) {
                    // Hide "more" button
                    if (response.status === 400) {
                        $el.wrapper.find('.js-news--button-more').addClass('hidden');
                    }
                }
            });
        };

        return {
            preloadNews: preloadNews
        }
    })();


    $(document).ready(function () {
        if ($el.wrapper.length === 0) {
            return;
        }

        $el.buttonMore.on('click', function (e) {
            newsModule.preloadNews();
            e.preventDefault();
        });

        newsModule.preloadNews();
    });
})(jQuery);