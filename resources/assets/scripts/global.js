'use strict';

(function ($) {
    var $header = $('.js-header');
    
    // Variables for beams on f1 page;
    var $beams = $('.beams__item');
    var beamsTransitionsValues = [];        

    $(document).ready(function () {
        // Smooth scroll
        $('a[href*=\\#].js-smooth-scroll-to').on('click', function (e) {
            var headerHeight = $header.outerHeight();
            var scrollTop = $(this.hash).offset().top;

            if (window.innerWidth >= 992) {
                scrollTop -= headerHeight;
            }

            scrollTop < 0 ? scrollTop = 0 : false;

            e.preventDefault();
            $('html, body').animate({
                scrollTop: scrollTop
            }, 500);
        });

        // Trailers
        var $trailerContainer = $('.trailer__container');
        $trailerContainer.slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 300,
            adaptiveHeight: true,
            arrows: false
        });

        var $trailerNav = $('.trailer__nav');
        $trailerNav.on('click', '.js-trailer--prev, .js-trailer--next', function (e) {
            console.log('kllklksdcsdc');
            if ($(this).hasClass('js-trailer--prev')) {
                $trailerContainer.slick('prev');
            } else if ($(this).hasClass('js-trailer--next')) {
                $trailerContainer.slick('next');
            }
            e.preventDefault();
        });


        // Animation on scroll
        AOS.init({
            duration: 300,
            easing: 'ease-in-sine',
            delay: 0,
            once: true,
            disable: window.innerWidth < 992
        });


        // // particles.js
        // particlesJS.load('particles-js', 'config/particlesjs-config.json');


        // Mobile menu
        var $mobileNavContainer = $('.js-mobile-nav--container');
        $mobileNavContainer.each(function () {
            var $mobileNav = $(this).find('.js-mobile-nav--nav');
            var $mobileNavSwitch = $(this).find('.js-mobile-nav--switch');
            var toggleNavigation = function () {
                $mobileNavSwitch.toggleClass('open');
                $mobileNav.slideToggle();
            };

            $mobileNavContainer.on('click', '.js-mobile-nav--switch', toggleNavigation);

            $mobileNavContainer.on('click', '.nav__link', function() {
                if ($mobileNavSwitch.hasClass('open')) {
                    toggleNavigation();
                }
            });
        });


        // Tabs
        $('.js-tabs').on('click', '.js-tabs-item', function (e) {
            var $tabs = $(this).closest('.js-tabs');
            var $selTab = $(this);
            e.preventDefault();

            $tabs.find('.tabs__switch-item--active').removeClass('tabs__switch-item--active');
            $selTab.addClass('tabs__switch-item--active');
        });


        // Popup
        $('body').on('click', '.js-open-popup', function (e) {
            $('body').addClass('popup__body');
        });
        $('body').on('click', '.js-close-popup', function (e) {
            $('body').removeClass('popup__body');
        });

        // Cookie Agreement
        var $cookie = $('.js-cookie');
        var isCookieAgreementChecked = localStorage.getItem('cookieAgreement');

        $cookie.toggleClass('cookie--show', !!!isCookieAgreementChecked);

        $cookie.on('click', '.js-cookie-dismiss', function (e) {
            localStorage.setItem('cookieAgreement', true);
            $cookie.removeClass('cookie--show');
        });

        // Press about us (Load more)
        var loadedMore = false;
        var loadPress;
        var timer;
        $('.press__more').on('click', function() {
            timer = loadedMore ? 250 : 0;

            loadPress = setTimeout(function() {
                $('.press__logos').toggleClass('press__logos--show');
                $('.press__logo').toggleClass('press__logo-details');
                $('.press__image').toggleClass('press__image--show');
                $('.press__read').toggle();
                $('.press__posted-on').toggle();
                $('.press--more').toggleClass('hidden');
                $('.press--less').toggleClass('hidden');
            }, timer);

            loadedMore = !loadedMore;
        });
        
        for (var i = 0; i < $beams.length; i++) {
            var count = Math.floor((Math.random()) * 200) + Math.floor((Math.random()) * 200);
            beamsTransitionsValues.push(count);
        };
    });

    // Light beams on f1 page - transitions functions
    function moveBeams(whichWay) {
        $.each($beams, function(index, beam) { 
            var topPositionOfBeam = $(beam).offset().top;
            var topPositionOfScreen = $(window).scrollTop();

            $(beam).css({ visibility: 'visible' });

            if (whichWay === 'down') {
                $(beam).transition({ rotate: '-45deg', x: '+=' + beamsTransitionsValues[index] + 'px', y: 0 }, 150, 'in-out');
                if (topPositionOfBeam < topPositionOfScreen) {
                    $(beam).transition({ rotate: '-45deg', x: -390 + 'px', y: 0 }, 1, 'out');
                    $(beam).css({ visibility: 'hidden' });
                }
            }
        });
    }

    var timeout = null;
    function throttle(func, whichWay, delay) {
        if (!timeout) {
          timeout = setTimeout(function() {
            func(whichWay);
            timeout = null;
          }, delay)
        }
    }

    var actuallScrollbarPosition = $(window).scrollTop();

    $(window).scroll(function () {
        var scrollTop = $(this).scrollTop();
        if (scrollTop > 150) {
            $header.addClass('header--slim');
        } else if (scrollTop < 80) {
            $header.removeClass('header--slim');
        }

        if ((typeof $beams === 'object') && ($beams.length > 0)) {
            if (scrollTop > actuallScrollbarPosition) {
                throttle(moveBeams, 'down', 200);
            } else if (scrollTop < actuallScrollbarPosition) {
                throttle(moveBeams, 'up', 200);
            }
        }

        actuallScrollbarPosition = scrollTop;
    });
    $(window).trigger('scroll');


    function displayConferencePopup() {
        if (localStorage.getItem('shouldHideConferencePopup') === 'yes') return;
        $('#conference-popup-trigger').click();
    }

    $('body').on('click', '.js-hide-conference-popup', function (e) {
        localStorage.setItem('shouldHideConferencePopup', 'yes');
    });
})(jQuery);
