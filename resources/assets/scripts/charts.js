'use strict';

(function ($) {
    var config = {
        yobit: {
            api: '//api.blockchain24.co/fto/data',
            label: 'Yobit price (USD)',
            prepareData: function(response) {
                var data = response.data;
                return data.map(function (item) {
                    return {
                        value: item.price,
                        label: moment.utc(item.updated_at).local().format('HH:mm'),
                        volume: item.vol
                    }
                });
            }
        },
        southxchange: {
            api: '//api.blockchain24.co/fto/data/southx',
            label: 'SouthXchange price (USD)',
            prepareData: function(response) {
                var data = response.data;
                return data.map(function (item) {
                    return {
                        value: item.price,
                        label: moment.utc(item.updated_at).local().format('HH:mm'),
                        volume: item.vol
                    }
                });
            }
        },
        coindeal: {
            api: '//api.blockchain24.co/fto/data/coindeal',
            label: 'Coindeal price (USD)',
            prepareData: function(response) {
                var data = response.data;
                return data.map(function (item) {
                    return {
                        value: item.price,
                        label: moment.utc(item.updated_at).local().format('HH:mm'),
                        volume: item.vol
                    }
                });
            }
        },
        coinbe: {
            api: '//api.blockchain24.co/fto/data/coinbe',
            label: 'Coinbe price (USD)',
            prepareData: function(response) {
                var data = response.data;
                return data.map(function (item) {
                    return {
                        value: item.price,
                        label: moment.utc(item.updated_at).local().format('HH:mm'),
                        volume: item.vol
                    }
                });
            }
        },
        exrates: {
            api: '//api.blockchain24.co/fto/data/exrates',
            label: 'Exrates price (USD)',
            prepareData: function(response) {
                var data = response.data;
                return data.map(function (item) {
                    return {
                        value: item.price,
                        label: moment.utc(item.updated_at).local().format('HH:mm'),
                        volume: item.vol
                    }
                });
            }
        },
        bitbay: {
            api: '//api.blockchain24.co/fto/data/bitbay',
            label: 'BitBay price (USD)',
            prepareData: function(response) {
                var data = response.data;
                return data.map(function (item) {
                    return {
                        value: item.price,
                        label: moment.utc(item.updated_at).local().format('HH:mm'),
                        volume: item.vol
                    }
                });
            }
        },
    };

    var availableMarkets = Object.keys(config);
    var selectedMarket = availableMarkets[0];

    var chartsData = {
        yobit: {
            labels: [],
            datasets: [{
                fill: false,
                borderColor: '#0198D0',
                backgroundColor: '#0198D0',
                data: []
            }]
        },
        southxchange: {
            labels: [],
            datasets: [{
                fill: false,
                borderColor: '#0198D0',
                backgroundColor: '#0198D0',
                data: []
            }]
        }
    };

    var chart = new Chart($('#market-performance-chart'), {
        type: 'line',
        data: chartsData[selectedMarket],
        options: {
            responsive: true,
            maintainAspectRatio: false,

            tooltips: {
                mode: 'index',
                intersect: false
            },
            elements: {
                line: {
                    tension: 0.000001
                }
            },
            plugins: {
                filler: {
                    propagate: false
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            }
        }
    });


    var drawChart = function (data, selectedMarket) {
        chart.data.datasets[0].data = [];
        chart.data.labels = [];
        chart.data.datasets[0].label = config[selectedMarket].label;

        data.forEach(function (item) {
            addChartValue(item);
        });
        chart.update();
    };


    var addChartValue = function(item) {
        item.value = Number(item.value).toFixed(2);
        chart.data.datasets[0].data.push(item.value);
        chart.data.labels.push(item.label);
    };


    var formatPrice = function (price, currency) {
        var priceToCompare = price;
        var priceFormatted = {
            value: price,
            currency: currency,
            unit: ''
        };

        ['', 'K', 'M', 'G'].some(function (unit) {
            if (priceToCompare < 1) {
                return true;
            } else {
                priceFormatted.value = priceToCompare;
                priceFormatted.unit = unit;

                priceToCompare /= 1000;
            }
        });

        return priceFormatted.currency + Number(priceFormatted.value).toFixed(2) + priceFormatted.unit;
    };


    var getApiData = function (selectedMarket) {
        $.ajax({
            type: 'GET',
            url: config[selectedMarket].api,

            success: function (response) {
                var data = config[selectedMarket].prepareData(response);
                var current = data[data.length - 1];

                drawChart(data, selectedMarket);

                // Update price boxes
                $('[data-fto-price]').text(formatPrice(current.value, '$'));
                $('[data-fto-volume]').text(formatPrice(current.volume, '$'));
            },

            error: function () {
                var response = {data: []};
                var data = config[selectedMarket].prepareData(response);
                var current = data[data.length - 1];

                drawChart(data, selectedMarket);

                // Update price boxes
                $('[data-fto-price]').text(formatPrice(0, '$'));
                $('[data-fto-volume]').text(formatPrice(0, '$'));
            }
        });
    };

    getApiData(selectedMarket);

    // Switch tab
    $('.js-tabs').on('click', '[data-chart-display]', function () {
        var selMarket = $(this).data('chart-display');
        if (availableMarkets.indexOf( selMarket ) !== -1) {
            // Display selected market
            getApiData(selMarket);
        }
    });

    // Add current price to Header
    (function () {
        var maxPrice = 0;
        var updateHeaderPrice = function (price) {
            maxPrice < price ? maxPrice = price : false;
            $('.js-header-price').text(formatPrice(maxPrice, '$'));
        };

        // Check only yobit and coindeal markets
        ['yobit', 'coindeal'].forEach(function (marketKey) {
            if (config[marketKey] && config[marketKey].api) {
                $.ajax({
                    type: 'GET',
                    url: config[marketKey].api,

                    success: function (response) {
                        var lastPrice = config[marketKey].prepareData(response).pop().value;
                        updateHeaderPrice(lastPrice);
                    },

                    error: function () {
                        updateHeaderPrice(0)
                    }
                });
            }
        });
    })();

}(jQuery));