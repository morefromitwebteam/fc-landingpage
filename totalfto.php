<?php
    
$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, "https://explorer.futurocoin.com/insight-api-dash/blocks?limit=1"); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
$block = curl_exec($ch); 
curl_close($ch);

$block = json_decode($block, true);
$blockheight =  $block["blocks"][0]["height"];
$circulate = number_format((($blockheight * 13.31811263) + 30000000), 8, '.', '');
exit($circulate);